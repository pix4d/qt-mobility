/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Mobility Components.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
** This file is part of the Ovi services plugin for the Maps and
** Navigation API.  The use of these services, whether by use of the
** plugin or by other means, is governed by the terms and conditions
** described by the file OVI_SERVICES_TERMS_AND_CONDITIONS.txt in
** this package, located in the directory containing the Ovi services
** plugin source code.
**
****************************************************************************/

#include "qgeomapreply_tiles.h"
#include "qgeotiledmaprequest.h"
#include <QGeoCoordinate>
#include <QNetworkAccessManager>
#include <QNetworkCacheMetaData>
#include <QDateTime>
#include <QImage>
#include <QtCore>
#include <QtGui>

QGeoMapReplyTiles::QGeoMapReplyTiles(QGeoTiledMapData* data, QString tilesDirectory, const QGeoTiledMapRequest &request, QObject *parent)
  : QGeoTiledMapReply(request, parent)
{
  int ymax = 1 << request.zoomLevel();
  int y = ymax - request.row() -1;

  QString tileName = tilesDirectory + QString::number(request.zoomLevel()) + "/" + QString::number(request.column()) + "/" + QString::number(y) + ".png";

  QFileInfo finfo(tileName);

  QByteArray ba;
  QImage im;
  //create white tile if not found
  if(!finfo.exists())
  {
    im = QImage(256,256,QImage::Format_RGB32);
    im.fill(QColor(255,255,255).rgba());
  }
  else
  {
    im = QImage(finfo.filePath());
  }

  QBuffer buffer(&ba);
  buffer.open(QIODevice::WriteOnly);
  im.save(&buffer,"PNG");

  setMapImageData(ba);
  setMapImageFormat("PNG");
  setFinished(true);
  setCached(false);

}

QGeoMapReplyTiles::~QGeoMapReplyTiles()
{
}


QNetworkReply* QGeoMapReplyTiles::networkReply() const
{
    return m_reply;
}

void QGeoMapReplyTiles::abort()
{
    if (!m_reply)
        return;

    m_reply->abort();
}

void QGeoMapReplyTiles::replyDestroyed()
{
    m_reply = 0;
}

void QGeoMapReplyTiles::networkFinished()
{
    if (!m_reply)
        return;

    qDebug() << "QGeoMapReplyTiles::networkFinished " << m_reply->error();

    if (m_reply->error() != QNetworkReply::NoError && m_reply->error() != QNetworkReply::ContentNotFoundError)
        return;

    QFileInfo whiteFile;
    QByteArray ba;
    if(m_reply->error() == QNetworkReply::ContentNotFoundError)
    {
      qDebug() << "create white tile";
      QImage im(256,256,QImage::Format_RGB32);
      im.fill(QColor(255,255,255).rgba());
      QBuffer buffer(&ba);
      buffer.open(QIODevice::WriteOnly);
      im.save(&buffer,"PNG");
      QFileInfo finfo(m_reply->url().toLocalFile());
      whiteFile.setFile(finfo.absolutePath()+"/white.png");
      im.save(whiteFile.filePath());
    }

    QVariant fromCache = m_reply->attribute(QNetworkRequest::SourceIsFromCacheAttribute);
    setCached(fromCache.toBool());

    if (!isCached()) {
      qDebug() << "not cached ";
        QAbstractNetworkCache *cache = m_reply->manager()->cache();
        if (cache) {
          QNetworkCacheMetaData metaData;
          if(m_reply->error() == QNetworkReply::ContentNotFoundError)
            metaData = cache->metaData(QUrl::fromLocalFile(whiteFile.filePath()));
          else
            metaData = cache->metaData(m_reply->url());
          QDateTime exp = QDateTime::currentDateTime();
          exp = exp.addDays(14);
          metaData.setExpirationDate(exp);
          cache->updateMetaData(metaData);
        }
    }


    if(whiteFile.exists())
    {
     QDir dir = whiteFile.absoluteDir();
     dir.remove(whiteFile.fileName());
    }
    if(m_reply->error() == QNetworkReply::ContentNotFoundError)
      setMapImageData(ba);
    else
      setMapImageData(m_reply->readAll());
    setMapImageFormat("PNG");
    setFinished(true);

    m_reply->deleteLater();
    m_reply = 0;
}

void QGeoMapReplyTiles::networkError(QNetworkReply::NetworkError error)
{
    if (!m_reply)
        return;

    if (error != QNetworkReply::OperationCanceledError && error != QNetworkReply::ContentNotFoundError)
        setError(QGeoTiledMapReply::CommunicationError, m_reply->errorString());

    //tiles do not exist : create white tile
    if(error == QNetworkReply::ContentNotFoundError)
      return;

    setFinished(true);
    m_reply->deleteLater();
    m_reply = 0;
}
