include(../../../features/utils.pri)

TEMPLATE = lib
CONFIG += plugin
TARGET = $$mobilityPluginTarget(qtgeoservices_tiles)
PLUGIN_TYPE=geoservices

include(../../../common.pri)

QT += network
QT += xml

CONFIG += mobility
MOBILITY = location

HEADERS += \
            qgeocodexmlparser.h \
            marclanguagecodes.h \
			qgeomappingmanagerengine_tiles.h \
			qgeomapreply_tiles.h \
			qgeoserviceproviderplugin_tiles.h

SOURCES += \
            qgeocodexmlparser.cpp \
			qgeomappingmanagerengine_tiles.cpp \
			qgeomapreply_tiles.cpp \
			qgeoserviceproviderplugin_tiles.cpp

INCLUDEPATH += $$SOURCE_DIR/src/location \
                $$SOURCE_DIR/src/location/maps \
                $$SOURCE_DIR/src/location/maps/tiled \
                $$SOURCE_DIR/include/QtLocation
