/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Mobility Components.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
** This file is part of the Ovi services plugin for the Maps and
** Navigation API.  The use of these services, whether by use of the
** plugin or by other means, is governed by the terms and conditions
** described by the file OVI_SERVICES_TERMS_AND_CONDITIONS.txt in
** this package, located in the directory containing the Ovi services
** plugin source code.
**
****************************************************************************/

#include "qgeomappingmanagerengine_tiles.h"
#include "qgeomapreply_tiles.h"

#include <qgeotiledmaprequest.h>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QSize>
#include <QDir>
#include <QUrl>
#include <QDebug>

#define LARGE_TILE_DIMENSION 256

#define DISK_CACHE_MAX_SIZE 50*1024*1024  //50MB

#define DISK_CACHE_ENABLED 1


QGeoMappingManagerEngineTiles::QGeoMappingManagerEngineTiles(const QMap<QString, QVariant> &parameters, QGeoServiceProvider::Error *error, QString *errorString)
        : QGeoTiledMappingManagerEngine(parameters)
{
    Q_UNUSED(error)
    Q_UNUSED(errorString)


    QVariant vMinZoom = parameters["minZoomLevel"];
    QVariant vMaxZoom = parameters["maxZoomLevel"];

    setTileSize(QSize(256, 256));
    setMinimumZoomLevel(vMinZoom.toReal());
    setMaximumZoomLevel(vMaxZoom.toReal());

    QList<QGraphicsGeoMap::MapType> types;
    types << QGraphicsGeoMap::StreetMap;
    types << QGraphicsGeoMap::SatelliteMapDay;
    types << QGraphicsGeoMap::TerrainMap;
    setSupportedMapTypes(types);

    QList<QGraphicsGeoMap::ConnectivityMode> modes;
    modes << QGraphicsGeoMap::OfflineMode;
    setSupportedConnectivityModes(modes);

    m_networkManager = new QNetworkAccessManager(this);

#ifdef DISK_CACHE_ENABLED
    QString cacheDir;
    if (parameters.contains("mapping.cache.directory"))
        cacheDir = parameters.value("mapping.cache.directory").toString();

    if (cacheDir.isEmpty()) {
        cacheDir = QDir::temp().path()+"/maptiles";
    }
    if (!cacheDir.isEmpty()) {
        m_cache = new QNetworkDiskCache(this);
        QDir dir;
        dir.mkpath(cacheDir);
        dir.setPath(cacheDir);

        m_cache->setCacheDirectory(dir.path());

        if (parameters.contains("mapping.cache.size")) {
            bool ok = false;
            qint64 cacheSize = parameters.value("mapping.cache.size").toString().toLongLong(&ok);
            if (ok)
                m_cache->setMaximumCacheSize(cacheSize);
        }

        if (m_cache->maximumCacheSize() > DISK_CACHE_MAX_SIZE)
            m_cache->setMaximumCacheSize(DISK_CACHE_MAX_SIZE);

        m_networkManager->setCache(m_cache);
    }
#endif


    QVariant v = parameters["tilesDirectory"];
    tilesDirectory = v.toString();
}

QGeoMappingManagerEngineTiles::~QGeoMappingManagerEngineTiles() {}

QGeoMapData* QGeoMappingManagerEngineTiles::createMapData()
{
    data = new QGeoTiledMapData(this);
    if (!data)
        return 0;

    data->setConnectivityMode(QGraphicsGeoMap::OfflineMode);
    return (QGeoMapData*)data;
}

QGeoTiledMapReply* QGeoMappingManagerEngineTiles::getTileImage(const QGeoTiledMapRequest &request)
{
  QGeoTiledMapReply* mapReply = new QGeoMapReplyTiles(data, tilesDirectory, request);

  return mapReply;

}


