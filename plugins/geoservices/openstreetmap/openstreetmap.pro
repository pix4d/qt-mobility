include(../../../features/utils.pri)

TEMPLATE = lib
CONFIG += plugin
TARGET = $$mobilityPluginTarget(qtgeoservices_osm)
PLUGIN_TYPE=geoservices

include(../../../common.pri)

QT += network
QT += xml

CONFIG += mobility
MOBILITY = location

HEADERS += \
            qgeomappingmanagerengine_osm.h \
            qgeomapreply_osm.h \
            qgeoserviceproviderplugin_osm.h \
            qgeocodeparser.h \
            qgeosearchreply_osm.h \
            qgeosearchmanagerengine_osm.h \
            parseproxy.h \
            qgeoroutingmanagerengine_osm.h \
            qgeoroutereply_osm.h \
            routeparser.h

SOURCES += \
            qgeomappingmanagerengine_osm.cpp \
            qgeomapreply_osm.cpp \
            qgeoserviceproviderplugin_osm.cpp \
            qgeocodeparser.cpp \
            qgeosearchreply_osm.cpp \
            qgeosearchmanagerengine_osm.cpp \
            qgeoroutingmanagerengine_osm.cpp \
            qgeoroutereply_osm.cpp \
            routeparser.cpp
			
INCLUDEPATH += $$SOURCE_DIR/src/location \
				$$SOURCE_DIR/src/location/maps \
				$$SOURCE_DIR/src/location/maps/tiled \
				$$SOURCE_DIR/include/QtLocation

